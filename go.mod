module gitlab.com/gitlab-org/gitlab-elasticsearch-indexer

go 1.18

require (
	github.com/aws/aws-sdk-go v1.44.220
	github.com/deoxxa/aws_signing_client v0.0.0-20161109131055-c20ee106809e
	github.com/go-enry/go-enry/v2 v2.8.4
	github.com/olivere/elastic/v7 v7.0.32
	github.com/stretchr/testify v1.8.1
	gitlab.com/gitlab-org/gitaly/v14 v14.10.5
	gitlab.com/gitlab-org/labkit v1.18.0
	gitlab.com/lupine/icu v1.0.0
	golang.org/x/net v0.8.0
	golang.org/x/tools v0.7.0
	google.golang.org/grpc v1.53.0
)

require (
	github.com/beevik/ntp v0.3.0 // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/client9/reopen v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-enry/go-oniguruma v1.2.1 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0 // indirect
	github.com/hashicorp/yamux v0.1.1 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.4 // indirect
	github.com/oklog/ulid/v2 v2.1.0 // indirect
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/prometheus/client_golang v1.14.0 // indirect
	github.com/prometheus/client_model v0.3.0 // indirect
	github.com/prometheus/common v0.42.0 // indirect
	github.com/prometheus/procfs v0.9.0 // indirect
	github.com/sebest/xff v0.0.0-20210106013422-671bd2870b3a // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	golang.org/x/mod v0.9.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
	google.golang.org/genproto v0.0.0-20230306155012-7f2fa6fef1f4 // indirect
	google.golang.org/protobuf v1.29.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

exclude (
	// https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer/-/issues/81
	github.com/gin-gonic/gin v1.4.0
	github.com/gin-gonic/gin v1.5.0
	github.com/gin-gonic/gin v1.6.0
	github.com/gin-gonic/gin v1.6.3
)
